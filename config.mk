# dwm version
VERSION = 6.1

# Customize below to fit your system

# paths
PREFIX = /usr
MANPREFIX = ${PREFIX}/share/man

X11INC = /usr/include/X11
X11LIB = /usr/lib/X11

# Xinerama, comment if you don't want it
XINERAMALIBS  = -lXinerama
XINERAMAFLAGS = -DXINERAMA

# includes and libs
INCS = -I${X11INC}
LIBS = -L${X11LIB} -lX11 ${XINERAMALIBS}

# flags
CFLAGS   += -g -march=x86-64 -mtune=native -O2 -pipe -fstack-protector -D_FORTIFY_SOURCE=2 -DVERSION=\"${VERSION}\" -pedantic -std=c99 ${INCS} ${XINERAMAFLAGS}
LDFLAGS  += -s ${LIBS}

# compiler and linker
CC = clang
